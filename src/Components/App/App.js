import React, { useState } from "react";
import './App.css';
import FormDemo from '../FormDemo/FormDemo'
import WelcomeDemo from '../WelcomeDemo/WelcomeDemo'
import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles({
  root: {
    boxSizing: "border-box",
    margin: '12px'
  }
});

function App() {
  const classes = useStyles();
  const [start, setStart] = useState(false);

  const toggleScreen = () => setStart(!start);

  if(!start) {
    return (
      <WelcomeDemo className={classes.root} toggleScreen={toggleScreen} />
    );
  }

  return (
    <FormDemo className={classes.root} toggleScreen={toggleScreen} />
  )

}

export default App;
