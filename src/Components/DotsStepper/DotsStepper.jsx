import React from "react";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import MobileStepper from "@material-ui/core/MobileStepper";
import IconButton from "@material-ui/core/IconButton";
import Icon from '@material-ui/core/Icon';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowAltCircleRight, faArrowCircleLeft, faArrowCircleRight } from "@fortawesome/free-solid-svg-icons";

const useStyles = makeStyles({
    root: {
        width: props => props.stepperWidth,
        flexGrow: 1,
        boxSizing: "border-box"
    },
    dotActive: {
        backgroundColor: "#61dafb"
    },
    dot: {
        marginRight: "6px"
    }
});

export default function DotsMobileStepper(props) {
  const classes = useStyles(props);
  const theme = useTheme();
  const [activeStep, setActiveStep] = React.useState(0);

  const handleNext = () => {
    props.toggleBack();
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const handleBack = () => {
      setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };


  return (
    <MobileStepper
      variant="dots"
      steps={8}
      position="static"
      activeStep={activeStep}
      className={classes.root}
      classes={{ dotActive: classes.dotActive, dot: classes.dot }}
      nextButton={
        <IconButton size="small" className={classes.stepButtons} onClick={handleNext} variant="outlined" disabled={activeStep === 7}>
          {theme.direction === "rtl" ? (
            <FontAwesomeIcon icon={faArrowCircleLeft}/>
          ) : (
            <FontAwesomeIcon icon={faArrowCircleRight} style={{color: activeStep === 7 ? "" : "#61dafb"}}/>
          )}
        </IconButton>
      }
      backButton={
        <IconButton size="small" className={classes.stepButtons} onClick={handleBack} disabled={activeStep === 0}>
          {theme.direction === "rtl" ? (
            <FontAwesomeIcon icon={faArrowCircleRight}/>
          ) : (
            <FontAwesomeIcon icon={faArrowCircleLeft} style={{color: activeStep === 0 ? "" : "#61dafb"}}/>
          )}
        </IconButton>
      }
    />
  );
}
