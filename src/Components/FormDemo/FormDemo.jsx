import { Avatar, Card, makeStyles, Typography, Button, Divider } from "@material-ui/core";
import React, { useState } from "react";
import InputField from "../InputDemo/InputDemo";
import DotsStepper from "../DotsStepper/DotsStepper"
import './FormDemo.css'

const fields = [
  "First Name",
  "Last Name",
  "Mobile Number",
  "Personal Email Address"
];

const FORM_WIDTH = '400px'

const useStyles = makeStyles({
  root: {
    width: FORM_WIDTH,
    padding: "20px",
    boxSizing: "border-box"
  },
  content: {
    display: "flex",
    flexDirection: "column",
    marginTop: "20px",
   
  },
  avatar: {
    width: '125px',
    height: '125px',
    margin: '0 auto'
  },
  buttons: {
    display: "flex",
    justifyContent: "space-between"
  },
  header: {
    display: "flex",
    flexDirection: "column",
    width: FORM_WIDTH,
    padding: "20px",
    boxSizing: "border-box"
  }
});

export default function DemoForm(props) {
  const classes = useStyles();
  const [disabledBack, setDisabledBack] = useState(true);

  const handleBack = () => props.toggleScreen();

  const toggleDisableBack = () => setDisabledBack(false);
  return (
    <>
      <section className={classes.header}>
        <Typography variant="h5" align="left">Your information</Typography>
        <Typography variant="body2" align="left">Add a photo and confirm your personal information</Typography>
        <Divider style={{marginTop: "24px"}}/>
      </section>
      <DotsStepper stepperWidth={FORM_WIDTH} toggleBack={toggleDisableBack} />
      <Card className={classes.root}>
        <div className={classes.content}>
          <Avatar src='' className={classes.avatar}/>
          <Typography variant="caption"align="left" style={{color: "#61dafb",margin: "0 auto", marginTop: "12px", marginBottom: "24px"}}>
            Edit Photo
          </Typography>
          {fields.map((field) => (
            <InputField  key={field} fieldName={field} />
          ))}
        </div>
        <div className={classes.buttons}>
          <Button color="primary" variant="outlined" disabled={disabledBack} onClick={handleBack}>Back</Button>
          <Button style={{backgroundColor: "#61dafb", color:"#fff"}} variant="contained">Next</Button>
        </div>
      </Card>
    </>
  );
}
