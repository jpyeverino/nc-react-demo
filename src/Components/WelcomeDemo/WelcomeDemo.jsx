import React from "react";
import { Avatar, Card, makeStyles, Typography, Button, Divider, List, ListItem, ListItemText } from "@material-ui/core";
import { AvatarGroup } from '@material-ui/lab';

const fields = [
  "First Name",
  "Last Name",
  "Mobile Number",
  "Personal Email Address"
];

const FORM_WIDTH = '400px'

const useStyles = makeStyles({
  root: {
    width: FORM_WIDTH,
    padding: "20px",
    boxSizing: "border-box"
  },
  content: {
    display: "flex",
    flexDirection: "column",
    marginTop: "20px",
   
  },
  avatarGroup: {
    margin: '0 auto',
    marginBottom: '24px'
  },
  avatar: {
    width: '125px',
    height: '125px'
  },
  buttons: {
    display: "flex",
    flexDirection: "row-reverse",
    justifyContent: "space-between"
  },
  header: {
    display: "flex",
    flexDirection: "column",
    width: FORM_WIDTH,
    padding: "15px",
    boxSizing: "border-box"
  }
});

const actions = [
  "Your contact information",
  "A PayPal or Venmo account for payment",
  "Two emergency contacts",
  "Photos of your valid driver's license",
  "Photos of your vehicle",
  "Certificate of Insurance (optional)"
];

export default function DemoWelcome(props) {
  const classes = useStyles();

  const handleNext = (e) => {
    e.preventDefault();
    props.toggleScreen();
  };

  return (
    <>
      <section className={classes.header}>
        <Typography variant="h5" align="left">Welcome to Pronto!</Typography>
        <Typography variant="body1" align="justify">You've been invited to join the Pronto Home Repairs team!</Typography>
        <Divider style={{marginTop: "24px", marginBottom: "24px"}}/>
        <Typography variant="body2" align="center">Below is a list of items you'll need to join</Typography>
      </section>
      
      <Card className={classes.root}>
        <div className={classes.content}>
          <AvatarGroup className={classes.avatarGroup}>
            <Avatar className={classes.avatar} alt="Dude Equis" src="/static/images/avatar/dude.jpg" />
            <Avatar className={classes.avatar} alt="Pronto Team" src="/static/images/avatar/avatar.png" />
          </AvatarGroup>
          <Typography variant="h5">Erik has invited you to Pronto!</Typography>
          <Typography variant="body2">Below is a list of items you'll need to join</Typography>
          <List>
            {actions.map((action, index) =>{
              const labelId = `list-label-${index}`;

              return (
                <>
                  <ListItem key={`action${new Date().getMilliseconds}`} role={undefined} dense >
                    <ListItemText id={labelId} primary={`${index + 1}. ${action}`} />
                  </ListItem>
                  {index < actions.length -1 ? <Divider component="li" /> : null }
                </>
              )
            })}
          </List>
        </div>
        <div className={classes.buttons}>
          <Button style={{backgroundColor: "#61dafb", color:"#fff"}} variant="contained" onClick={handleNext}>Next</Button>
        </div>
      </Card>
    </>
  );
}
