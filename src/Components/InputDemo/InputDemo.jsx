import React from "react";
import {
  InputBase,
  InputLabel,
  FormControl,
  Typography
} from "@material-ui/core";
import {
  fade,
  ThemeProvider,
  withStyles,
  makeStyles,
  createMuiTheme
} from "@material-ui/core/styles";

const InputField = withStyles((theme) => ({
  root: {
    "label + &": {
      marginTop: theme.spacing(3)
    }
  },
  input: {
    marginBottom: "12px",
    borderRadius: 2,
    position: "relative",
    backgroundColor: theme.palette.common.white,
    border: "1px solid #ced4da",
    fontSize: 16,
    width: "100%",
    padding: "10px 12px",
    transition: theme.transitions.create(["border-color", "box-shadow"]),
    // Use the system font instead of the default Roboto font.
    fontFamily: [
      "-apple-system",
      "BlinkMacSystemFont",
      '"Segoe UI"',
      "Roboto",
      '"Helvetica Neue"',
      "Arial",
      "sans-serif",
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"'
    ].join(","),
    "&:focus": {
      boxShadow: `${fade("#ff0000", 0.25)} 0 0 0 0.2rem`,
      borderColor: theme.palette.primary.main
    }
  }
}))(InputBase);

export default function DemoInput(props) {
  const { fieldName } = props;
  return (
    <FormControl>
      <InputLabel shrink htmlFor={fieldName}>
        <Typography variant="h6">{fieldName}</Typography>
      </InputLabel>
      <InputField defaultValue={fieldName} id={fieldName} />
    </FormControl>
  );
}
